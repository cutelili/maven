package com.aldebaran.web.controller;

import com.aldebaran.pojo.UsersMavenWebDemo;
import com.aldebaran.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 处理用户管理请求
 */
@Controller
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UsersService usersService;
    /**
     * 处理添加用户请求
     */
    @RequestMapping("/addUsers")
    public String addUsers(UsersMavenWebDemo users){
        this.usersService.addUsers(users);
        /*完成用户添加后为了防止表单重复提交，使用重定向的方式来产生相应，请求一个ok.jsp*/
        return "redirect:/ok";
    }
}
