package com.aldebaran.mapper;

import com.aldebaran.pojo.UsersMavenWebDemo;
import com.aldebaran.pojo.UsersMavenWebDemoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UsersMavenWebDemoMapper {
    long countByExample(UsersMavenWebDemoExample example);

    int deleteByExample(UsersMavenWebDemoExample example);

    int deleteByPrimaryKey(Integer userid);

    int insert(UsersMavenWebDemo record);

    int insertSelective(UsersMavenWebDemo record);

    List<UsersMavenWebDemo> selectByExample(UsersMavenWebDemoExample example);

    UsersMavenWebDemo selectByPrimaryKey(Integer userid);

    int updateByExampleSelective(@Param("record") UsersMavenWebDemo record, @Param("example") UsersMavenWebDemoExample example);

    int updateByExample(@Param("record") UsersMavenWebDemo record, @Param("example") UsersMavenWebDemoExample example);

    int updateByPrimaryKeySelective(UsersMavenWebDemo record);

    int updateByPrimaryKey(UsersMavenWebDemo record);
}