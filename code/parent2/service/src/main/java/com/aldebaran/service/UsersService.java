package com.aldebaran.service;

import com.aldebaran.pojo.UsersMavenWebDemo;

public interface UsersService {
    //在controller下的applicationContext-trans.xml上有事务处理行为，所以add等开头的方法不能乱写
    void addUsers(UsersMavenWebDemo users);//UsersMavenWebDemo就是Users
}
