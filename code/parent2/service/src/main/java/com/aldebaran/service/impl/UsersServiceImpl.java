package com.aldebaran.service.impl;

import com.aldebaran.mapper.UsersMavenWebDemoMapper;
import com.aldebaran.pojo.UsersMavenWebDemo;
import com.aldebaran.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户管理业务层
 */
@Service
public class UsersServiceImpl implements UsersService {

    //注入UsersMapper
    @Autowired
    private UsersMavenWebDemoMapper usersMapper;

    /**
     * 用户添加业务
     * @param users
     */
    @Override
    //事务控制
    @Transactional
    public void addUsers(UsersMavenWebDemo users) {
        this.usersMapper.insert(users);
    }
}
