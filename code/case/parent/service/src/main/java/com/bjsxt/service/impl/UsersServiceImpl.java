package com.bjsxt.service.impl;

import com.bjsxt.mapper.UsersMapper;
import com.bjsxt.pojo.Users;
import com.bjsxt.pojo.UsersExample;
import com.bjsxt.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 用户管理业务层
 */
@Service
public class UsersServiceImpl implements UsersService {
    @Autowired
    private UsersMapper usersMapper;

    /**
     * 用户添加业务
     * @param users
     */
    @Override
    @Transactional
    public void addUsers(Users users) {
        this.usersMapper.insert(users);
    }

    /**
     * 查询全部用户
     * @return
     */
    @Override
    public List<Users> findUsers() {
        UsersExample example = new UsersExample();
        return this.usersMapper.selectByExample(example);
    }

    /**
     * 根据用户ID查询用户
     * @param userid
     * @return
     */
    @Override
    public Users findUserById(Integer userid) {
        return this.usersMapper.selectByPrimaryKey(userid);
    }

    /**
     * 更新用户
     * @param users
     */
    @Override
    @Transactional
    public void modifyUsers(Users users) {
        this.usersMapper.updateByPrimaryKey(users);
    }

    /**
     * 根据用户ID删除用户
     * @param userid
     */
    @Override
    @Transactional
    public void dropUsersById(Integer userid) {
        this.usersMapper.deleteByPrimaryKey(userid);
    }

}
